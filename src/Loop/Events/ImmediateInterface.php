<?php

/*
 * This file is part of Icicle, a library for writing asynchronous code in PHP using promises and coroutines.
 *
 * @copyright 2014-2015 Aaron Piotrowski. All rights reserved.
 * @license MIT See the LICENSE file that was distributed with this source code for more information.
 */

namespace Icicle\Loop\Events;

interface ImmediateInterface extends EventInterface
{
    /**
     * @return bool
     */
    public function isPending(): bool;

    /**
     * Execute the immediate if not pending.
     */
    public function execute();

    /**
     * Cancels the immediate if pending.
     */
    public function cancel();

    /**
     * Calls the callback associated with the immediate.
     */
    public function call();

    /**
     * Alias of call().
     */
    public function __invoke();
}
